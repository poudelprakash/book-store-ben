class Cart < ApplicationRecord
  belongs_to :user
  has_many :cart_items
  has_many :books, through: :cart_items

  enum status: [:in_progress, :order_placed, :abandoned]

  def add_book(book)
    cart_items.create(book: book)
  end

  def remove_book(book)
    cart_items.where(book_id: book.id).last.destroy
  end

  def place_order
    order_placed!
  end
end
