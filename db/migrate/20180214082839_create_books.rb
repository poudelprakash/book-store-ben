class CreateBooks < ActiveRecord::Migration[5.1]
  def change
    create_table :books do |t|
      t.string :title
      t.string :isbn
      t.string :author
      t.float :price
      t.boolean :is_available, default: true
      t.integer :total_orders, default: 0

      t.timestamps
    end
  end
end
