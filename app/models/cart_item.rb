class CartItem < ApplicationRecord
  belongs_to :cart
  belongs_to :book, counter_cache: :total_orders

  scope :total_books_sold, -> {joins(:cart).where('carts.status=1').count}
end
