class BookDetailSerializer < ActiveModel::Serializer
  attributes :id, :title, :isbn, :author, :price, :total_orders

  def total_orders
    object.cart_items.count
  end
end
