class CartSerializer < ActiveModel::Serializer
  attributes :id, :status
  has_one :user
  has_many :books
end
