class BookIndexSerializer < ActiveModel::Serializer
  attributes :total_books_sold
  has_many :top_selling_books, serializer: BookDetailSerializer

  def total_books_sold
    CartItem.total_books_sold
  end

  def top_selling_books
    Book.best_sellers.first(10)
  end
end
