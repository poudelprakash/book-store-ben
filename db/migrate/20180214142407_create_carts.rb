class CreateCarts < ActiveRecord::Migration[5.1]
  def change
    create_table :carts do |t|
      t.integer :status, default: 0 # Enum (0:in_progress, 1:order_placed, 2:abandoned) default in_progress

      t.belongs_to :user, foreign_key: true

      t.timestamps
    end
  end
end
