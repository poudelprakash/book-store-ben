class User < ApplicationRecord
  has_secure_password

  has_many :carts

  has_many :cart_items, through: :carts do
    def bought
      where('carts.status = 1')
    end
  end

  has_many :books, through: :cart_items do
    def bought
      where('carts.status = 1')
    end
  end

  def bought_books
    books.bought
  end

  def amount_spent
    books.bought.sum(:price)
  end

  def total_orders
    books.bought.count
  end

  def top_n_books_bought_available(n)
    books.bought.where(is_available: true).last n
  end
end
