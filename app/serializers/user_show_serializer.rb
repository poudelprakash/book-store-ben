class UserShowSerializer < ActiveModel::Serializer
  attributes :id, :email, :first_name, :last_name, :total_orders, :amount_spent

  has_one :top_five_books

  def total_orders
    object.total_orders
  end

  def amount_spent
    object.amount_spent
  end

  def top_five_books
    object.top_n_books_bought_available(5)
  end
end
