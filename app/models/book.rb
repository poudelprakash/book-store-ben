class Book < ApplicationRecord
  has_many :cart_items
  has_many :carts, through: :cart_items

  scope :search_by_name_author_isbn, -> (search) {where("title LIKE ? OR author LIKE ? OR isbn LIKE ?", "%#{search}%", "%#{search}%", "%#{search}%")}

  scope :best_sellers, -> {
    includes(:carts).where("carts.status=1").references(:carts).order('total_orders DESC')
  }
end
