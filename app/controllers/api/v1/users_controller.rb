module Api
  module V1
    class UsersController < ApiController
      before_action :set_user, only: [:show]

      # GET /api/v1/users
      def index
        @users = User.all
        render json: @users, each_serializer: UserIndexSerializer
      end

      # GET /api/v1/users/1
      def show
        render json: @user, serializer: UserShowSerializer
      end

      private
      def set_user
        @user = User.find(params[:id])
      end
    end
  end
end
