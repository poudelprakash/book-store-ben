User.create(id: 1, first_name: 'Steve', last_name: 'Jobs', email: 'steve@example.com', password:'steve1234', password_confirmation: 'steve1234')
User.create(id: 2, first_name: 'Lynda', last_name: 'Jobs', email: 'lynda@example.com', password:'steve1234', password_confirmation: 'steve1234')
User.create(id: 3, first_name: 'Lisa', last_name: 'Jobs', email: 'lisa@example.com', password:'steve1234', password_confirmation: 'steve1234')

learning_ruby = Book.create(id: 1, title: 'Learning Ruby', isbn: ' 978-0596529864', author: 'Michael Fox', price: 200)
learning_js = Book.create(id: 2, title: 'Learning JS', isbn: ' 978-0596529865', author: 'Brendan Eich', price: 200)
html_book = Book.create(id: 3, title: 'HTML', isbn: '978-0596529865', author: 'Tim Lee', price: 200)
Book.create(id: 4, title: 'The Jungle Book', isbn: '978-0596529867', author: 'Michael Fox', price: 200)
Book.create(id: 5, title: 'The Monk', isbn: '978-0596529867', author: 'Dalai Lama', price: 200)
Book.create(id: 6, title: 'The Startup', isbn: '978-0596529867', author: 'Eric Ries', price: 200)
unavailable_book = Book.create(id: 7, title: 'Remote', isbn: ' 978-0596529867', author: 'David Heinemeier Hansson', price: 200, is_available: false)

steve_abandoned_cart = Cart.create(user_id: 1, status: :abandoned)
steve_cart = Cart.create(user_id: 1, status: :order_placed)
lynda_cart = Cart.create(user_id: 2)

steve_cart.add_book(learning_ruby)
steve_cart.add_book(learning_js)
steve_cart.add_book(unavailable_book)

lynda_cart.add_book(html_book)