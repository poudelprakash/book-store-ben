class UserIndexSerializer < ActiveModel::Serializer
  attributes :id, :email, :first_name, :last_name, :total_orders, :amount_spent, :current_cart

  def total_orders
    object.total_orders
  end

  def amount_spent
    object.amount_spent
  end

  def current_cart
    cart = object.carts.in_progress.last
    (cart && cart.cart_items) ? cart : {}
  end

end
