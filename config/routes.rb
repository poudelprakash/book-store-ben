Rails.application.routes.draw do
  
  post 'user_token' => 'user_token#create'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api, defaults: {format: :json} do
    namespace :v1 do
      resources :books, only: [:index] do
        collection do
          get '/search/:query', to: 'books#search'
        end
      end

      resources :users, only: [:index, :show]
      resources :carts, only: [:index]
    end
  end
end
