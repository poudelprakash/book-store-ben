module Api
  module V1
    class BooksController < ApiController
      def index
          @books = Book.all
          render json: {}, serializer: BookIndexSerializer
      end

      def search
        @books = Book.search_by_name_author_isbn(params[:query])
        render json: @books, each_serializer: BookSerializer
      end
    end
  end
end

