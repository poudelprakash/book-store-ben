module Api
  module V1
    class CartsController < ApiController

      # GET /api/v1/carts
      def index
        @carts = Cart.abandoned
        render json: @carts
      end
    end
  end
end